//
// Copyright (C) 2020 Luca Giacomoni and George Parisis
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#ifndef INET_LEARNING_RLINTERFACE_RLINTERFACE_H_
#define INET_LEARNING_RLINTERFACE_RLINTERFACE_H_

#include <omnetpp.h>

#include "../../common/rl/rlUtil.h"

using namespace omnetpp;

namespace learning {

class RLInterface : public cListener
{
protected:
    // signal that carries stepData
    simsignal_t dataSignal;

    // signal that carries an action query
    simsignal_t querySignal;

    // string is used to identify the interface within this simulation
    std::string stringId;

    // the closest parent cSimpleModule used to emit signals (and have access to cSimpleModule methods)
    cComponent *owner;

    // variables to keep track of the transitioning of states and the action taken
    State rlOldState, rlState;

    int stateSize;

    float lastMiAction;

    double delayWeightReward;

public:
    ~RLInterface();
    void setOwner(cComponent *_owner); // sets the owner pointer. must be called in the initialize method
    void setStringId(std::string _id);
    void setMaxObservationCount(int size); // sets the max number of observations that make a state

    void initialize(int stateSize, int obsMaxSize);

    void updateState(Observation obs); //updates the state by adding the observation obs

    virtual void receiveSignal(cComponent *source, simsignal_t signalID, cObject *obj, cObject *details) override; // callback for receiving action response

    float computeReward(float delta, float delay, float throughput); // computes reward

    // must be implemented by concrete RLInterface classes
    virtual void cleanup() = 0;
    virtual void decisionMade(float action) = 0; // defines what to do when decision is made

    int getStateSize() const;
    void setStateSize(int stateSize);
};

} //namespace learning

#endif /* INET_LEARNING_RLINTERFACE_RLINTERFACE_H_ */
