//
// Copyright (C) 2020 Luca Giacomoni and George Parisis
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#ifndef PROJECT_AGENT_H
#define PROJECT_AGENT_H

#include <omnetpp.h>
#include <torch/torch.h>

#include "../common/cOrnsteinUhlenbeck.h"

#include "DdpgModel.h"
#include "../common/rl/rlUtil.h"
#include "ReplayBuffer.h"

using namespace omnetpp;

#ifdef PARALLEL_EXEC

#else
ReplayBuffer replayBuffer;
Actor actorLocal = nullptr;
Actor actorTarget = nullptr;
std::shared_ptr<torch::optim::Adam> actorOptimizerPtr;

Critic criticLocal1 = nullptr;
Critic criticLocal2 = nullptr;

Critic criticTarget1 = nullptr;
Critic criticTarget2 = nullptr;

std::shared_ptr<torch::optim::Adam> criticOptimizerPtr1;
std::shared_ptr<torch::optim::Adam> criticOptimizerPtr2;

long stepNumber;         // Counter for number of learning steps performed so far

#endif

namespace learning {

using Experience = tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>;
using ExperienceVec = tuple<vector<torch::Tensor>, vector<torch::Tensor>, vector<torch::Tensor>,vector<torch::Tensor>, vector<torch::Tensor>>;

/*
 * Implements the training logic of the algorithm. It manages the Actor and Critic models.
 */
class Agent : public cSimpleModule, public cListener
{
private:
    bool isEvaluation;      // Flag for training and evaluation mode
    int saveFrequency;      // Number of steps to save the policy
    int episodeNumber;      // If training, it stores the current episode number. If evaluation, it stores the numbger of epiosde whose policy has been loaded to evaluate.
    std::string policyPath; // Directory where policies are stored
    int stateSize;          // Number of features of the state
    int actionSize;         // Number of components of the action
    int batchSize;          // Mini-batch size
    double gamma;           // Discount factor gamma
    double tau;             // Rate by which the prediction model shifts to the target model


    std::shared_ptr<mapped_region> shmRegionPtr;
    std::string regionName;

    simsignal_t actionResponse;

    std::shared_ptr<torch::Device> devicePtr; // Sets whether to use CPU or GPU
    std::shared_ptr<cRandom> noisePtr; // Noise to be applied to the action.

    simsignal_t cLossSignal;
    simsignal_t aLossSignal;
    simsignal_t actionNoNoiseSignal;
    simsignal_t actionWithNoiseSignal;
    simsignal_t noiseSignal;

#ifdef PARALLEL_EXEC
    ReplayBuffer replayBuffer;

    Actor actorLocal = nullptr;
    Actor actorTarget = nullptr;

    std::shared_ptr<torch::optim::Adam> actorOptimizerPtr;

    Critic criticLocal = nullptr;
    Critic criticTarget = nullptr;

    std::shared_ptr<torch::optim::Adam> criticOptimizerPtr;
#endif

    void softUpdateActor(Actor local, Actor target, double tau);
    void softUpdateCritic(Critic local, Critic target, double tau);
    void hardUpdateActor(Actor local, Actor target);
    void hardUpdateCritic(Critic local, Critic target);

protected:
    virtual void initialize() override;

    // this clears up the warnings about hiding overloaded functions - I think in a correct way
    // both cSimpleModule and cListener have a finish() function but with different signatures
    using cListener::finish;
    virtual void finish() override;
public:

    ~Agent();

    void initialiseForEvaluation();

    // callback for when signals (step data and action request) are received
    void receiveSignal(cComponent *src, simsignal_t id, cObject *value, cObject *details) override;

    // called at every time step, when new step information is available.
    void step(vector<float> state, vector<float> action, float reward, vector<float> nextState, bool done);

    // compute next action (for both training and evaluation)
    // it takes a state as input and a boolean to toggle noise addition on the output action.
    vector<float> act(vector<float> state, bool addNoise);

    // perform learning
    void learn(Experience batch);

    // save and load a policy by episode number
    void saveCheckpoint(int e);
    void loadCheckpoint(int e);

    // save and load policy and optimizer as default location (for multi-process training)
    void saveCheckpoint();
    void loadCheckpoint();

    // save replay buffer (for multi-process training)
    void saveReplayBuffer();
    // load replay buffer (for multi-process training)
    void loadReplayBuffer();

    // for debugging purposes
    std::string computeHashOfWeights(std::vector<torch::Tensor> _weights);

    // convert a tuple of vectors of tensors into a tuple of tensors (used to generate mini-batches of training data)
    Experience processBatch(ExperienceVec preBatch); //merge vector of tensors into single tensors to be fed to the NN
};
}

#endif //PROJECT_AGENT_H

