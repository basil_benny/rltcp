//
// Copyright (C) 2020 Luca Giacomoni and George Parisis
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#ifndef PROJECT_REPLAYBUFFER_H
#define PROJECT_REPLAYBUFFER_H

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/circular_buffer.hpp>

// this is required here to fix a clash with macosx event.h file that defines EV_ERROR as 0x400
#undef EV_ERROR
#include <omnetpp.h>
#define EV_ERROR EV_LOG(omnetpp::LOGLEVEL_ERROR, nullptr)

#include <ATen/core/Formatting.h>

#include "../common/rl/rlUtil.h"

// Experience consists of state, action, reward, next_state, done
using Experience = std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>;
using ExperienceVec = std::tuple<std::vector<torch::Tensor>, std::vector<torch::Tensor>, std::vector<torch::Tensor>,std::vector<torch::Tensor>, std::vector<torch::Tensor>>;

using namespace omnetpp;
using namespace boost::interprocess;

/*
 * Fixed size circular buffer used to store steps of experience. Used to sample training batches.
 */
class ReplayBuffer
{
public:
    std::shared_ptr<boost::circular_buffer<Experience> > replayBuffer;
    std::shared_ptr<mapped_region> shmRegionPtr;

    void initialise(int _size);
    void initialise(int _size, std::string _memSegmentName);

    void addExperienceState(torch::Tensor state, torch::Tensor action, torch::Tensor reward, torch::Tensor nextState, torch::Tensor done);

    void addExperienceState(Experience experience);

    //return a single experience point
    Experience sample();

    //return a mini-batch of size n of experience
    ExperienceVec sampleBatch(int n);

    size_t getLength();

    long int randomInt(long int _right);

    void saveReplayBuffer();

    void loadReplayBuffer();

    void deSerializeFromManagedMemory(std::string *replayBufferAsString, size_t replayBufferAsStringSize);

};

#endif //PROJECT_REPLAYBUFFER_H

