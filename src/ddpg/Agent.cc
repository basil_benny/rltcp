//
// Copyright (C) 2020 Luca Giacomoni and George Parisis
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include "Agent.h"

#define UPDATE_FREQ 5

namespace learning {

Define_Module(Agent);

void Agent::initialize() {
    cSimpleModule::initialize();

    stateSize = par("stateSize");
    actionSize = par("actionSize");

    batchSize = par("batchSize");

    isEvaluation = par("isEvaluation");
    saveFrequency = par("saveFrequency");
    policyPath = par("policyPath").stdstringValue();
    actionResponse = registerSignal("actionResponse");
    episodeNumber = par("episodeNumber");

    gamma = par("gamma");
    tau = par("tau");

    /***** IMPORTANT *****/
    /* bufferSize, lrActor, lrCritic are NOT read here for the non PARALLEL_EXEC version but in the main */

    // Noise related parameters
    double _outheta = par("OUTheta");
    double _oumu = par("OUMu");
    double _ousigma = par("OUSigma");
    double _oudt = par("OUDt");
    double _oux0 = par("OUX0");
    double _noiseDecay = par("noiseDecayFactor");
    if (!isEvaluation)
        regionName = par("regionName").stdstringValue();

#ifdef PARALLEL_EXEC
    int _bufferSize = par("bufferSize");
    double _lrActor = par("lrActor");
    double _lrCritic = par("lrCritic");

    actorLocal = Actor(stateSize, actionSize);
    if (episodeNumber == 0)
        actorLocal->resetParameters();

    actorTarget = Actor(stateSize, actionSize);
    actorOptimizerPtr = make_shared<torch::optim::Adam>(actorLocal->parameters(), _lrActor);

    criticLocal = Critic(stateSize, actionSize);
    if (episodeNumber == 0)
        criticLocal->resetParameters();

    criticTarget = Critic(stateSize, actionSize);
    criticOptimizerPtr = make_shared<torch::optim::Adam>(criticLocal->parameters(), _lrCritic);
#endif

    if (torch::cuda::is_available()) {
        EV_INFO << "Agent - Cuda available" << endl;
        devicePtr = make_shared<torch::Device>(torch::kCUDA);
        actorLocal->to(*devicePtr);
        criticLocal1->to(*devicePtr);
        criticLocal2->to(*devicePtr);
        actorTarget->to(*devicePtr);
        criticTarget1->to(*devicePtr);
        criticTarget2->to(*devicePtr);
    }
    else {
        EV_INFO << "Agent - CPU used" << endl;
        devicePtr = make_shared<torch::Device>(torch::kCPU);
    }

    cLossSignal = registerSignal("criticLoss");
    aLossSignal = registerSignal("actorLoss");
    actionNoNoiseSignal = registerSignal("actionNoNoise");
    actionWithNoiseSignal = registerSignal("actionWithNoise");
    noiseSignal = registerSignal("noise");

    if (isEvaluation) {
        initialiseForEvaluation();
        noisePtr = nullptr;
        getSimulation()->getSystemModule()->subscribe("actionQuery", (cListener*) this);           // subscribe to signal actionQuery
        loadCheckpoint(episodeNumber);
    }
    else {
#ifdef PARALLEL_EXEC
        shared_memory_object shmObject(open_or_create, regionName.c_str(), read_write);
        shmRegionPtr = std::make_shared<mapped_region>(shmObject, read_write);

        replayBuffer.initialise(_bufferSize, regionName);
        replayBuffer.loadReplayBuffer();
        if (episodeNumber > 0) {
            loadCheckpoint();
        }
#endif
        // noisePtr = std::make_shared<cOrnsteinUhlenbeck>(_outheta, _oumu, _ousigma, _oudt, _noiseDecay, _oux0);
        noisePtr = std::make_shared<cNormal>(getRNG(0), 0, 0.2);
        getSimulation()->getSystemModule()->subscribe("actionQuery", (cListener*) this);           //Subscribe to signal actionQuery
        getSimulation()->getSystemModule()->subscribe("stepData", (cListener*) this);
        EV_INFO << "Buffer Length: " << replayBuffer.getLength() << std::endl;
        if (episodeNumber == 0) {
            hardUpdateActor(actorLocal, actorTarget);
            hardUpdateCritic(criticLocal1, criticTarget1);
            hardUpdateCritic(criticLocal2, criticTarget2);
        }
    }
}

void Agent::initialiseForEvaluation() {
    actorLocal = Actor(stateSize, actionSize);
    criticLocal1 = Critic(stateSize, actionSize);
}

vector<float> Agent::act(vector<float> state, bool addNoise) {
    torch::Tensor torchState = torch::tensor(state, torch::dtype(torch::kFloat)).to(*devicePtr);
    actorLocal->eval();
    auto action = actorLocal->forward(torchState).to(torch::kCPU);
    actorLocal->train();
    vector<float> v(action.data_ptr<float>(), action.data_ptr<float>() + action.numel());
    EV_TRACE << "****************** action ****************" << std::endl;
    if (addNoise) {
        for (size_t i = 0; i < (size_t) actionSize; i++) {
            if (isnan(v[i]))
                throw cRuntimeError("Agent returned a NaN action");
            emit(actionNoNoiseSignal, v[i]);
            EV_TRACE << "Action (without noise): " << v << std::endl;
            double t = noisePtr->draw();
            t = fmin(fmax(t, -0.5), 0.5);
            EV_TRACE << "Noise: " << t << endl;
            emit(noiseSignal, t);
            v[i] += t;
            emit(actionWithNoiseSignal, v[i]);
            EV_TRACE << "Action (with noise before clipping): " << v << std::endl;
            v[i] = fmin(fmax(v[i], 0.f), 1.f);
        }
    }
    EV_TRACE << "Final action: " << v << std::endl;
    return v;
}

void Agent::step(vector<float> state, vector<float> action, float reward, vector<float> nextState, bool done) {
    std::vector<float> rewardVec { reward };
    std::vector<float> doneVec { (float) done };
    torch::Tensor _state = torch::tensor(state, torch::dtype(torch::kFloat)).to(*devicePtr);
    torch::Tensor _action = torch::tensor(action, torch::dtype(torch::kFloat)).to(*devicePtr);
    torch::Tensor _reward = torch::tensor(rewardVec, torch::dtype(torch::kFloat)).to(*devicePtr);
    torch::Tensor _nextState = torch::tensor(nextState, torch::dtype(torch::kFloat)).to(*devicePtr);
    torch::Tensor _done = torch::tensor(doneVec, torch::dtype(torch::kFloat)).to(*devicePtr);

    replayBuffer.addExperienceState(_state, _action, _reward, _nextState, _done);

    // start learning, when enough samples are available in memory
    if (replayBuffer.getLength() > (size_t) batchSize) {
        Experience batch;
        if (batchSize == 1) {
            batch = replayBuffer.sample();
        }
        else if (batchSize > 1) {
            ExperienceVec preBatch = replayBuffer.sampleBatch(batchSize);
            batch = processBatch(preBatch);
        }
        else {
            throw cRuntimeError("Batch size has to be >= 1");
        }
        // Use the experience to learn
        learn(batch);
    }
    else {
        EV_TRACE << "Not enough experience to start learning (" << replayBuffer.getLength() << "/" << batchSize << ")" << std::endl;
    }
}

Experience Agent::processBatch(ExperienceVec preBatch) {
    auto statesVec = get<0>(preBatch);
    auto actionsVec = get<1>(preBatch);
    auto rewardsVec = get<2>(preBatch);
    auto nextStatesVec = get<3>(preBatch);
    auto doneVec = get<4>(preBatch);

    torch::Tensor statesTensor = torch::stack( { statesVec.at(0), statesVec.at(1) });
    for (auto it = statesVec.begin() + 2; it != statesVec.end(); ++it) {
        statesTensor = torch::cat( { statesTensor, it->view( { 1, stateSize }) });
    }

    torch::Tensor actionsTensor = torch::stack( { actionsVec.at(0), actionsVec.at(1) });
    for (auto it = actionsVec.begin() + 2; it != actionsVec.end(); ++it) {
        actionsTensor = torch::cat( { actionsTensor, it->view( { 1, actionSize }) });
    }

    torch::Tensor rewardsTensor = torch::stack( { rewardsVec.at(0), rewardsVec.at(1) });
    for (auto it = rewardsVec.begin() + 2; it != rewardsVec.end(); ++it) {
        rewardsTensor = torch::cat( { rewardsTensor, it->view( { 1, 1 }) });
    }

    torch::Tensor nextStatesTensor = torch::stack( { nextStatesVec.at(0), nextStatesVec.at(1) });
    for (auto it = nextStatesVec.begin() + 2; it != nextStatesVec.end(); ++it) {
        nextStatesTensor = torch::cat( { nextStatesTensor, it->view( { 1, stateSize }) });
    }

    torch::Tensor doneTensor = torch::stack( { doneVec.at(0), doneVec.at(1) });
    for (auto it = doneVec.begin() + 2; it != doneVec.end(); ++it) {
        doneTensor = torch::cat( { doneTensor, it->view( { 1, 1 }) });
    }

    return make_tuple(statesTensor, actionsTensor, rewardsTensor, nextStatesTensor, doneTensor);
}

void Agent::learn(Experience batch) {
    stepNumber++;
    auto state = get<0>(batch);
    auto action = get<1>(batch);
    auto reward = get<2>(batch);
    auto nextState = get<3>(batch);
    auto done = get<4>(batch);

    // ---------------------------- update critic ---------------------------- #
    torch::Tensor nextAction = actorTarget->forward(nextState);
    torch::Tensor noise = torch::clamp(torch::randn_like(nextAction)*0.2, -0.5,0.5);
    torch::Tensor targetNextAction = torch::clamp((nextAction + noise),0,1);

    auto targetQ1 = criticTarget1->forward(nextState, targetNextAction);
    auto targetQ2 = criticTarget2->forward(nextState, targetNextAction);

    torch::Tensor targetQ = at::min(targetQ1,targetQ2);

    auto y = reward + (gamma * targetQ * (1 - done));

    auto q1 = criticLocal1->forward(state, action);
    auto q2 = criticLocal2->forward(state, action);

    torch::Tensor criticLoss = torch::mse_loss(q1, y.detach()) + torch::mse_loss(q2, y.detach());

    criticOptimizerPtr1->zero_grad();
    criticOptimizerPtr2->zero_grad();

    criticLoss.backward();

    criticOptimizerPtr1->step();
    criticOptimizerPtr2->step();

    if (stepNumber % UPDATE_FREQ == 0) {

        // ---------------------------- update actor ---------------------------- #
        auto nextAction = actorLocal->forward(state);
        auto actorLoss = -criticLocal1->forward(state, nextAction).mean(); // TODO: not a loss - what is a better name - how do people call it

        actorOptimizerPtr->zero_grad();
        actorLoss.backward();
        actorOptimizerPtr->step();

        // move to CPU if they were on GPU
        auto criticLossCPU = criticLoss.to(torch::kCPU);
        auto actorLossCPU = actorLoss.to(torch::kCPU);

        // record scalars
        vector<float> cLoss(criticLossCPU.data_ptr<float>(), criticLossCPU.data_ptr<float>() + criticLossCPU.numel());
        vector<float> aLoss(actorLossCPU.data_ptr<float>(), actorLossCPU.data_ptr<float>() + actorLossCPU.numel());

        emit(cLossSignal, cLoss[0]);
        emit(aLossSignal, aLoss[0]);

        // soft update target networks
        softUpdateCritic(criticLocal1, criticTarget1, tau);
        softUpdateCritic(criticLocal2, criticTarget2, tau);
        softUpdateActor(actorLocal, actorTarget, tau);
    }
}

void Agent::softUpdateActor(Actor local, Actor target, double tau) {
//    Soft update model parameters.
//    θ_target = τ*θ_local + (1 - τ)*θ_target

    for (size_t i = 0; i < target->parameters().size(); i++) {
        torch::NoGradGuard noGrad;
        target->parameters()[i].copy_(tau * local->parameters()[i] + (1.0 - tau) * target->parameters()[i]);
    }
}

void Agent::softUpdateCritic(Critic local, Critic target, double tau) {
//    Soft update model parameters.
//    θ_target = τ*θ_local + (1 - τ)*θ_target

    for (size_t i = 0; i < target->parameters().size(); i++) {
        torch::NoGradGuard noGrad;
        target->parameters()[i].copy_(tau * local->parameters()[i] + (1.0 - tau) * target->parameters()[i]);
    }
}

void Agent::hardUpdateActor(Actor local, Actor target) {
    for (size_t i = 0; i < target->parameters().size(); i++) {
        torch::NoGradGuard noGrad;
        target->parameters()[i].copy_(local->parameters()[i]);
    }
}

void Agent::hardUpdateCritic(Critic local, Critic target) {
    for (size_t i = 0; i < target->parameters().size(); i++) {
        torch::NoGradGuard noGrad;
        target->parameters()[i].copy_(local->parameters()[i]);
    }
}

void Agent::saveCheckpoint(int eps) {
    auto fileActor(policyPath + "/ckp_actor_agent" + "_" + to_string(eps) + ".pt");
    auto fileCritic(policyPath + "/ckp_critic_agent" + "_" + to_string(eps) + ".pt");

    torch::save(actorLocal, fileActor);
    torch::save(criticLocal1, fileCritic);
    EV_TRACE << "Saved checkpoint for episode: " << eps << endl;
}

void Agent::loadCheckpoint(int eps) {
    auto fileActor(policyPath + "/ckp_actor_agent" + "_" + to_string(eps) + ".pt");
    auto fileCritic(policyPath + "/ckp_critic_agent" + "_" + to_string(eps) + ".pt");

    torch::load(actorLocal, fileActor);
    torch::load(criticLocal1, fileCritic);
    EV_TRACE << "Loaded checkpoint for episode: " << eps << endl;
}

void Agent::saveCheckpoint() {
    auto fileActorLocal(regionName + "actorLocal.pt");
    auto fileActorTarget(regionName + "actorTarget.pt");
    auto fileActorOpt(regionName + "actorOpt.pt");

    auto fileCriticLocal(regionName + "criticLocal.pt");
    auto fileCriticTarget(regionName + "criticTarget.pt");
    auto fileCriticOpt(regionName + "criticOpt.pt");

    torch::save(actorLocal, fileActorLocal);
    torch::save(actorTarget, fileActorTarget);
    torch::save(*actorOptimizerPtr, fileActorOpt);

    torch::save(criticLocal1, fileCriticLocal);
    torch::save(criticTarget1, fileCriticTarget);
    torch::save(*criticOptimizerPtr1, fileCriticOpt);

    EV_TRACE << "Saved full checkpoint - episode " << episodeNumber << endl;
}

void Agent::loadCheckpoint() {
    auto fileActorLocal(regionName + "actorLocal.pt");
    auto fileActorTarget(regionName + "actorTarget.pt");
    auto fileActorOpt(regionName + "actorOpt.pt");

    auto fileCriticLocal(regionName + "criticLocal.pt");
    auto fileCriticTarget(regionName + "criticTarget.pt");
    auto fileCriticOpt(regionName + "criticOpt.pt");

    torch::load(actorLocal, fileActorLocal);
    torch::load(actorTarget, fileActorTarget);
    torch::load(*actorOptimizerPtr, fileActorOpt);

    torch::load(criticLocal1, fileCriticLocal);
    torch::load(criticTarget1, fileCriticTarget);
    torch::load(*criticOptimizerPtr1, fileCriticOpt);

    EV_TRACE << "Loaded full checkpoint - episode " << episodeNumber << endl;
}

void Agent::saveReplayBuffer() {
    replayBuffer.saveReplayBuffer();
    EV_TRACE << "Saved replay buffer - episode " << episodeNumber << endl;
}

void Agent::loadReplayBuffer() {
    replayBuffer.loadReplayBuffer();
    EV_TRACE << "Loaded replay buffer - episode " << episodeNumber << endl;
}

void Agent::receiveSignal(cComponent *src, simsignal_t id, cObject *value, cObject *details) {
    const char *signalName = getSignalName(id);

    // requested action by an agent (TCP)
    if (strcmp(signalName, "actionQuery") == 0) {
        Query *_query = dynamic_cast<Query*>(value);
        EV_TRACE << "****************** action query ****************" << std::endl;
        EV_TRACE << _query->state.str() << std::endl;

        if (!_query->state.isValid()) {
            EV_WARN << "invalid state " << _query->state.str() << std::endl;
        }

        // extract a flat version of the state (in case there are multiple observations in a single state)
        vector<float> flatState = _query->state.flatten();

        // return a vector
        auto action_v = this->act(flatState, !isEvaluation);

        // single dimension action space, so take the first and only element
        float action = action_v[0];

        //Send the ation to the querying agent
        Response resp(_query->queryId, action);
        emit(actionResponse, &resp);
    }
    // step data send by an agent. Perform a single training step.
    else if (strcmp(signalName, "stepData") == 0) {
        if (!isEvaluation) {
            Step *step = dynamic_cast<Step*>(value);
            // step function of Agent requires a vector of actions as input, so initialize vector.
            std::vector<float> action_v = { step->a };
            EV_TRACE << "****************** step data ****************" << std::endl;
            EV_TRACE << step->str() << std::endl;
            if (!step->isValid()) {
                EV_WARN << "invalid step" << std::endl;
                EV_WARN << step->str() << std::endl;
            }
            // perform a single training step
            this->step(step->s.flatten(), action_v, step->r, step->s_p.flatten(), step->done);
        }
        else {
            EV_ERROR << "Step data signal received during evaluation" << std::endl;
            throw cRuntimeError("Received step data signal during evaluation.");
        }
    }
    else {
        EV_ERROR << "Unknown signal..aborting" << std::endl;
        throw cRuntimeError("Received unknown signal.");
    }
}

void Agent::finish() {
    if (!isEvaluation) {
#ifdef PARALLEL_EXEC
        saveReplayBuffer();
        saveCheckpoint();
#endif
        // save policy for EVALUATION purposes every saveFrequency episodes
        if (episodeNumber % saveFrequency == 0) {
            saveCheckpoint(episodeNumber);
        }
    }
}

Agent::~Agent() {
    EV_TRACE << "deleting Agent" << endl;
}

std::string Agent::computeHashOfWeights(std::vector<torch::Tensor> _weights) {
    std::vector<int> sizePattern;
    std::vector<torch::Tensor> flatWeights;
    for (torch::Tensor &t : _weights)
        flatWeights.push_back(t.reshape( { -1 }));

    for (torch::Tensor &t : flatWeights) {
        int dimension = t.ndimension();
        for (int i = 0; i < dimension; i++)
            sizePattern.push_back(t.size(i));
    }
    return learning::computeTensorVectorSHA(flatWeights, sizePattern);
}

}
