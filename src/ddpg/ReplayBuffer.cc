//
// Copyright (C) 2020 Luca Giacomoni and George Parisis
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include "ReplayBuffer.h"

unsigned long intRandForShuffle(long r)
{
    return intrand(omnetpp::getEnvir()->getRNG(0), r);
}

void ReplayBuffer::initialise(int _size)
{
    replayBuffer = std::make_shared<boost::circular_buffer<Experience> >(_size);
}

void ReplayBuffer::initialise(int _size, std::string _memSegmentName)
{
    EV_INFO << "Initialising from shared memory: " << _memSegmentName << std::endl;
    replayBuffer = std::make_shared<boost::circular_buffer<Experience> >(_size);
    shared_memory_object shmObject(open_or_create, _memSegmentName.c_str(), read_write);
    shmRegionPtr = std::make_shared<mapped_region>(shmObject, read_write);
}

void ReplayBuffer::addExperienceState(torch::Tensor state, torch::Tensor action, torch::Tensor reward, torch::Tensor nextState, torch::Tensor done)
{
    addExperienceState(std::make_tuple(state, action, reward, nextState, done));
}

void ReplayBuffer::addExperienceState(Experience experience)
{
    replayBuffer->push_back(experience);
}

Experience ReplayBuffer::sample()
{
    return replayBuffer->at(intrand(omnetpp::getEnvir()->getRNG(0), getLength()));
}

ExperienceVec ReplayBuffer::sampleBatch(int n)
{
    std::vector<torch::Tensor> states, actions, rewards, newStates, done;
    /*
     * Generate a list of unique indices. Method 1 is efficient when l>>n. More specifically when T < (l - 1)/l, where:
     * T = Sum[ 1/(l - k) ], k = 0 --> n - 1
     * n is the number of samples
     * l is the length of the buffer
     * Ref: https://stackoverflow.com/questions/6947612/generating-m-distinct-random-numbers-in-the-range-0-n-1
     * if l is close to n, then use method 2.
     */
    std::vector<int> indeces(n);

    // calculate T
    double T = 0;
    for (int k = 0; k < n; k++)
        T += (1.0 / (getLength() - k));

    if (T < ((getLength() - 1.0) / getLength())) {
        EV_TRACE << "using method 1 to generate random indices (T:" << T << ")" << std::endl;
        int totalClashes = 0;
        for (int i = 0; i < n; ++i) {
            int r;
            int clashes = -1;
            do {
                clashes++;
                r = intrand(omnetpp::getEnvir()->getRNG(0), getLength());
            } while (std::find(std::begin(indeces), std::end(indeces), r) != std::end(indeces));

            totalClashes = totalClashes + clashes;
            indeces[i] = r;
        }
        EV_TRACE << "clashes during random generation: " << totalClashes << std::endl;
    }
    else {
        EV_TRACE << "using method 2 to generate random indices(T:" << T << ")" << std::endl;
        std::vector<int> arr(getLength());
        for (size_t i = 0; i < getLength(); ++i)
            arr[i] = i;
        std::random_shuffle(std::begin(arr), std::end(arr), intRandForShuffle);
        for (int k = 0; k < n; k++) {
            indeces[k] = arr[k];
        }
    }
    for (int i = 0; i < n; i++) {
        Experience e = replayBuffer->at(indeces[i]);
        states.push_back(std::get<0>(e));
        actions.push_back(std::get<1>(e));
        rewards.push_back(std::get<2>(e));
        newStates.push_back(std::get<3>(e));
        done.push_back(std::get<4>(e));
    }
    return std::make_tuple(states, actions, rewards, newStates, done);
}

size_t ReplayBuffer::getLength()
{
    return replayBuffer->size();
}

void ReplayBuffer::saveReplayBuffer()
{
    std::vector<torch::Tensor> tensorVec;
    std::ostringstream stream;

    EV_INFO << "saving replay buffer to shared memory" << std::endl;
    for (size_t i = 0; i < replayBuffer->size(); i++) {
        Experience exp = replayBuffer->at(i);
        tensorVec.push_back(std::get<0>(exp));
        tensorVec.push_back(std::get<1>(exp));
        tensorVec.push_back(std::get<2>(exp));
        tensorVec.push_back(std::get<3>(exp));
        tensorVec.push_back(std::get<4>(exp));
    }
    torch::save(tensorVec, stream);

    //Get the number of characters of the replay buffer as string. Also add the null terminating character
    uint32_t replayBufferAsStringSize = stream.str().length();

    EV_TRACE << "Size of the replay buffer as string at termination of simulation: " << replayBufferAsStringSize << std::endl;

    //Copy size of the current buffer into the first 4 bytes of memory (int size)
    std::memcpy(shmRegionPtr->get_address(), &replayBufferAsStringSize, sizeof(replayBufferAsStringSize));

    //Copy the content of the string into the shared memory
    std::memcpy(((uint8_t*) shmRegionPtr->get_address() + sizeof(uint32_t)), stream.str().c_str(), replayBufferAsStringSize);
}

void ReplayBuffer::loadReplayBuffer()
{
    // Size in bytes of the string representing the replay buffer
    uint32_t replayBufferSize;

    EV_INFO << "loading replay buffer from shared memory" << std::endl;

    //Copy an integer from shared memory into our int value
    std::memcpy(&replayBufferSize, shmRegionPtr->get_address(), sizeof(uint32_t));

    if (replayBufferSize != 0) {
        //Create a string from the pointer to char * and specify the number of characters to be used to construct the string. If not specified, the string is constructed incorrectly.
        std::string replayBufferAsString((char*) ((uint8_t*) shmRegionPtr->get_address() + sizeof(uint32_t)), replayBufferSize);
        std::vector<torch::Tensor> tensorVec;
        std::istringstream stream(replayBufferAsString);

        torch::load(tensorVec, stream);
        // iterate 5 by 5 to create Experience tuple
        for (std::vector<torch::Tensor>::iterator it = tensorVec.begin(); it != tensorVec.end(); std::advance(it, 5)) {
            Experience exp = std::make_tuple(*it, *(it + 1), *(it + 2), *(it + 3), *(it + 4));
            replayBuffer->push_back(exp);
        }
    }
    else {
        EV_WARN << "Episode 0 - Replay Buffer starts afresh." << std::endl;
    }
}

