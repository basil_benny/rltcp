//
// Copyright (C) 2020 Luca Giacomoni and George Parisis
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include <math.h>

#include "DdpgModel.h"

/******************* ACTOR *******************/

ActorImpl::ActorImpl(int64_t stateSize, int64_t actionSize, int64_t fc1Units, int64_t fc2Units) :
        torch::nn::Module()
{
    //Register all modules (in sequence from input to output)
    //bnA1 = register_module("bnA1", torch::nn::BatchNorm1d(bnOptions(stateSize)));
    linAc1 = register_module("linAc1", torch::nn::Linear(stateSize, fc1Units));
    //bnA2 = register_module("bnA2", torch::nn::BatchNorm1d(bnOptions(fc1Units)));
    linAc2 = register_module("linAc2", torch::nn::Linear(fc1Units, fc2Units));
    //bnA3 = register_module("bnA3", torch::nn::BatchNorm1d(bnOptions(fc2Units)));
    linAc3 = register_module("linAc3", torch::nn::Linear(fc2Units, actionSize));
    bnA4 = register_module("bnA4", torch::nn::BatchNorm1d(bnOptions(actionSize)));

}

std::pair<double, double> ActorImpl::hiddenInit(torch::nn::Linear &layer)
{
    //Xavier initialization. The range from which weights are drawn depends on the number of parameters.
    double lim = 1. / sqrt(layer->weight.sizes()[0]);
    return std::make_pair(-lim, lim);
}

void ActorImpl::resetParameters()
{
    //Find the range for weights initialization
    auto fc1_init = hiddenInit(linAc1);

    //initialise weights of the networks
    torch::nn::init::uniform_(linAc1->weight, fc1_init.first, fc1_init.second);

    //Do the same for second layer
    auto fc2_init = hiddenInit(linAc2);
    torch::nn::init::uniform_(linAc2->weight, fc2_init.first, fc2_init.second);

    //TODO:: why are these weights fixed? Does have anything to do with the fact that this layer has a single output neuron?
    torch::nn::init::uniform_(linAc3->weight, -3e-3, 3e-3);
}

torch::Tensor ActorImpl::forward(torch::Tensor x)
{
    //Convert the form of [[x1, x2, ... , xn]], if the input Tensor is a single data point in the form of [x1, x2, ... , xn]
    if (x.dim() == 1)
        x = torch::unsqueeze(x, 0);

    //Call forward for each layer
    //x = bnA1->forward(x);
    x = linAc1->forward(x);
    //x = bnA2->forward(x);
    x = torch::relu(x);

    x = linAc2->forward(x);
    //x = bnA3->forward(x);
    x = torch::relu(x);

    x = linAc3->forward(x);
    //x = bnA4->forward(x);
    x = torch::sigmoid(x);

    return x;

}

torch::nn::BatchNormOptions ActorImpl::bnOptions(int64_t features)
{
    torch::nn::BatchNormOptions bn_options = torch::nn::BatchNormOptions(features);
    bn_options.affine(true);
    bn_options.track_running_stats(true);
    return bn_options;
}

/******************* Critic *****************/

CriticImpl::CriticImpl(int64_t stateSize, int64_t actionSize, int64_t fc1Units, int64_t fc2Units) :
        torch::nn::Module()
{
    //bnC1 = register_module("bnC1", torch::nn::BatchNorm1d(bnOptions(stateSize)));
    linCr1 = register_module("linCr1", torch::nn::Linear(stateSize, fc1Units));
    //bnC2 = register_module("bnC2", torch::nn::BatchNorm1d(bnOptions(fc1Units)));
    linCr2 = register_module("linCr2", torch::nn::Linear(fc1Units + actionSize, fc2Units));
    bnC3 = register_module("bnC3", torch::nn::BatchNorm1d(bnOptions(fc2Units)));
    linCr3 = register_module("linCr3", torch::nn::Linear(fc2Units, 1));
}

std::pair<double, double> CriticImpl::hiddenInit(torch::nn::Linear &layer)
{
    double lim = 1. / sqrt(layer->weight.sizes()[0]);
    return std::make_pair(-lim, lim);
}

void CriticImpl::resetParameters()
{
    auto fcs1_init = hiddenInit(linCr1);
    torch::nn::init::uniform_(linCr1->weight, fcs1_init.first, fcs1_init.second);
    auto fc2_init = hiddenInit(linCr2);
    torch::nn::init::uniform_(linCr2->weight, fc2_init.first, fc2_init.second);
    torch::nn::init::uniform_(linCr3->weight, -3e-3, 3e-3);
}

torch::Tensor CriticImpl::forward(torch::Tensor x, torch::Tensor action)
{
    if (x.dim() == 1)
        x = torch::unsqueeze(x, 0);

    if (action.dim() == 1)
        action = torch::unsqueeze(action, 0);

    //x = bnC1->forward(x);
    x = linCr1->forward(x);
    //x = bnC2->forward(x);
    x = torch::relu(x);

    x = torch::cat( { x, action }, /*dim=*/1);
    x = linCr2->forward(x);
    //x = bnC3->forward(x);
    x = torch::relu(x);

    return linCr3->forward(x);
}

torch::nn::BatchNormOptions CriticImpl::bnOptions(int64_t features)
{
    torch::nn::BatchNormOptions bn_options = torch::nn::BatchNormOptions(features);
    bn_options.affine(true);
    bn_options.track_running_stats(true);
    return bn_options;
}

