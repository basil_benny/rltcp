//
// Copyright (C) 2020 Luca Giacomoni and George Parisis
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

package mltcp.ddpg;

@namespace(learning);

module Agent
{
    parameters:
        @class(Agent);

        int stateSize;			// size of input
        int actionSize;			// just 1 for now

        int bufferSize;  		// replay buffer size
        int batchSize;         	// batch size
        
        double lrCritic;    	// learning rate of the critic
        double lrActor;			// learning rate of the actor
        double tau;				// update rate (local - target NNs)
        double gamma;			// RL discount factor
        
        int saveFrequency = default(100);
        
        bool isEvaluation = default(false);

		string policyPath = default("policies");
        string regionName = default("shared-mem");
        
        int episodeNumber;

        double OUTheta = default(1.0);
        double OUMu = default(0);
        double OUSigma = default(0.3);
        double OUDt = default(0.05);
        double OUX0 = default(0);
        double noiseDecayFactor = default(1);
        
        @signal[actionResponse];
        
         
//      Results recording related signals. 
        @signal[criticLoss];
        @signal[actorLoss];
        @signal[actionNoNoise];
        @signal[actionWithNoise];
        @signal[noise];
        
        @statistic[criticLoss](record=vector;);
        @statistic[actorLoss](record=vector;);
        @statistic[actionNoNoise](record=vector;);
        @statistic[actionWithNoise](record=vector;);
        @statistic[noise](record=vector;);

}
